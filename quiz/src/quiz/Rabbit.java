package quiz;

public class Rabbit {
	
	// -----------
	// PROPERTIES
	// -----------
	private int xPosition=250;
	private int yPosition=250;
	
	
	// -----------
	// CONSTRUCTOR 
	// ------------
	public Rabbit() {

	}
	
	// -----------
	// METHODS 
	// ------------
	public void printCurrentPosition() {
		System.out.println("The current position of the rabbit is: ");
	}

	public void sayHello() {
		System.out.println("Hello! I am a rabbit!");
	}
	
	// ----------------
	// ACCESSOR METHODS
	// ----------------
	
	// Put all your accessor methods in this section.
	
	
}

